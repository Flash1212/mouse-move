#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=..\Images\black mouse.ico
#AutoIt3Wrapper_Outfile=Mouse Move v2.0.exe
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#include <TrayConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>

Opt("TrayAutoPause", 0)
Opt("TrayMenuMode", 2)

#Region Declarations & Tray Settings
Global $DEBUG = 0
Global $Sleep = 180000
Global $Timer, $tDiff
Global $aMposStart, $aMposEnd, $x = 1, $y = 1
Global $fINI = @ScriptDir&'\MouseMove.ini'

;~ Global $trayPause = TrayCreateItem("Pause", -1, 0)
;~ 					TrayItemSetOnEvent(-1, "_Pause")
					TrayCreateItem("")
Global $traySleep = TrayCreateItem("Sleep Interval", -1, -1)
					TraySetOnEvent(-1, "_SleepTimer")
Global $trayPixel = TrayCreateItem("Pixels Moved", -1, -1)
					TraySetOnEvent(-1, "_PixelsMoved")
					TrayCreateItem("")
;~ Global $trayExit =  TrayCreateItem("Exit", -1, -1)
;~ 					TrayItemSetOnEvent(-1, "_Exit")
					TraySetState($TRAY_ICONSTATE_SHOW) ; Show the tray menu.
#EndRegion

$Timer = TimerInit()
_Main()

Func _Main()
	If FileExists($fINI) Then _rSettings()

	_Get_Move('Get') ;Set the current mouse position

	While 1

		If TimerDiff($Timer) >= $Sleep Then ;in miliseconds
			_Get_Move('Move'); Check if mouse needs to move
			_Get_Move('Get') ;Set the current mouse position
			$Timer = TimerInit()
		EndIf

		Switch TrayGetMsg()
			Case $traySleep
				_SleepTimer()
			Case $trayPixel
				_PixelsMoved()
		EndSwitch

	WEnd

EndFunc

Func _Get_Move($Action)

	If $DEBUG = 1 Then
		MsgBox(64,"DEBUG", $Action&" Module")
		Return
	ElseIf $DEBUG = 2 Then
		MsgBox(64,"DEBUG", $Action&" Module")
	Else
		ConsoleWrite("Get_Move" & @LF)
	EndIf

	If $Action = 'Get' Then
		$aMposStart = MouseGetPos()
		ConsoleWrite("X-Axis = "&$aMposStart[0]&@CRLF&"Y-Axis = "&$aMposStart[1]&@CRLF)
	ElseIf $Action = 'Move' Then
		$aMposEnd = MouseGetPos()
		If 	$aMposStart[0] = $aMposEnd[0] And $aMposStart[1] = $aMposEnd[1] Then
			MouseMove($aMposStart[0] + $x, $aMposStart[1] + $y)
		EndIf
		ConsoleWrite("X-Axis = "&$aMposEnd[0]&@CRLF&"Y-Axis = "&$aMposEnd[1]&@CRLF)
	Else
		ConsoleWrite("Unknown _Get_Move Action"&@CRLF)
		MsgBox(16, 'Error', "Unknown _Get_Move Action")
		Return
	EndIf

	ConsoleWrite("SleepTimer = "&$Sleep&@CRLF)


	ConsoleWrite("Get_Move complete."&@CRLF)

EndFunc
#cs
Func _Pause()
	If $DEBUG = 1 Then
		MsgBox(64,"DEBUG", "Pause Module")
		Return
	ElseIf $DEBUG = 2 Then
		MsgBox(64,"DEBUG", "Pause Module")
	Else
		ConsoleWrite("Pause" & @CRLF)
	EndIf

    TrayItemSetText($trayPause, "Resume")
    Local Static $bPaused = False
    $bPaused = Not $bPaused
    While 3
        Switch TrayGetMsg()
			Case $trayPause
				_Pause()
			Case $traySleep
				_SleepTimer()
			Case $trayPixel
				_PixelsMoved()
			Case $trayExit
				_Exit()
        EndSwitch
    WEnd
    TrayItemSetText($trayPause, "Pause")
    ConsoleWrite("Resume" & @CRLF)
EndFunc   ;==>_Pause
#ce
Func _SleepTimer()
	Local $fSleep, $UserDefSleep, $Sleep_Label1, $Sleep_Label2, $Sleep_Label3, $isleep

	If $DEBUG = 1 Then
		MsgBox(64,"DEBUG", "Sleep Timer Module")
		Return
	ElseIf $DEBUG = 2 Then
		MsgBox(64,"DEBUG", "Sleep Timer Module")
	Else
		ConsoleWrite("SleepTimer" & @LF)
	EndIf

	#Region ### START Koda GUI section ### Form=
	$fSleep = 		GUICreate("Sleep Timer", 326, 83, 769, 372, BitXOR($GUI_SS_DEFAULT_GUI, $WS_MINIMIZEBOX))

	$Sleep_Label1 = GUICtrlCreateLabel("Note: The defualt is set to 180 seconds (3 mins).", 8, 8, 249, 17)
					GUICtrlSetResizing(-1, $GUI_DOCKLEFT)
	$Sleep_Label2 = GUICtrlCreateLabel("*Note: The minimum entry for stable performance is 1 second.", 8, 32, 304, 17)
					GUICtrlSetResizing(-1, $GUI_DOCKLEFT)
	$UserDefSleep = GUICtrlCreateInput($Sleep/1000, 149, 52, 51, 21, BitOR($GUI_SS_DEFAULT_INPUT,$ES_NUMBER))
	$Sleep_Label3 = GUICtrlCreateLabel("Enter sleep time in seconds. ", 8, 56, 139, 17)
					GUICtrlSetResizing(-1, $GUI_DOCKLEFT)
					GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###

	While 1
		$nMsg = GUIGetMsg()
		Switch $nMsg
			Case $GUI_EVENT_CLOSE
				GUISetState(@SW_HIDE, $fSleep)
				ExitLoop
		EndSwitch
	WEnd

	$isleep = GUICtrlRead($userDefSleep)
	If $isleep < 1 Then $isleep = 1
	$Sleep = $isleep*1000

	_wSettings()
	ConsoleWrite("SleepTimer = "&$Sleep&@CRLF&"Sleep Timer Complete."&@CRLF)
EndFunc

Func _PixelsMoved()
	Local	$fPixel, $Pixel_Label1, $Pixel_Label2, $Pixel_Label3, $UserDefX, $UserDefY, $iX, $iY

	If $DEBUG = 1 Then
		MsgBox(64,"DEBUG", "Pixel Module")
		Return
	ElseIf $DEBUG = 2 Then
		MsgBox(64,"DEBUG", "Pixel Module")
	Else
		ConsoleWrite("Pixel" & @LF)
	EndIf

	#Region ### START Koda GUI section ### Form=C:\Users\DJThornton\Dropbox\Code\Mouse move\GUI\Pixels.kxf
	$fPixel = 		GUICreate("Pixel Move", 299, 83, 781, 372, BitXOR($GUI_SS_DEFAULT_GUI, $WS_MINIMIZEBOX))
	$Pixel_Label1 = GUICtrlCreateLabel("Note: that the defualt is set to 1 Pixel on both the X & Y axis.", 8, 8, 287, 17)
					GUICtrlSetResizing(-1, $GUI_DOCKLEFT)
	$Pixel_Label2 = GUICtrlCreateLabel("Entries are relative to current position.", 8, 32, 238, 17)
					GUICtrlSetResizing(-1, $GUI_DOCKLEFT)
	$UserDefX = 	GUICtrlCreateInput($x, 157, 52, 33, 21, BitOR($GUI_SS_DEFAULT_INPUT,$ES_NUMBER))
	$Pixel_Label3 = GUICtrlCreateLabel("Enter pixels in whole numbers. ", 8, 56, 149, 17)
					GUICtrlSetResizing(-1, $GUI_DOCKLEFT)
	$UserDefY = 	GUICtrlCreateInput($y, 211, 52, 33, 21, BitOR($GUI_SS_DEFAULT_INPUT,$ES_NUMBER))
	$Label1 = 		GUICtrlCreateLabel("X", 196, 55, 11, 17)
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###

	While 1
		$nMsg = GUIGetMsg()
		Switch $nMsg
			Case $GUI_EVENT_CLOSE
				GUISetState(@SW_HIDE, $fPixel)
				ExitLoop
		EndSwitch
	WEnd

	$x = Round(GUICtrlRead($UserDefX))

	$y = Round(GUICtrlRead($UserDefY))

	_wSettings()

	ConsoleWrite("X-Axis = "&$x&@CRLF&"Y-Axis = "&$y&@CRLF&"Pixels Complete."&@CRLF)
EndFunc

Func _rSettings()
	Local	$aINI

	If $DEBUG = 1 Then
		MsgBox(64,"DEBUG", "rSettings Module")
		Return
	ElseIf $DEBUG = 2 Then
		MsgBox(64,"DEBUG", "rSettings Module")
	Else
		ConsoleWrite("rSettings" & @LF)
	EndIf

	$aINI = IniReadSection($fINI, 'Settings')
	If @error Then
		MsgBox(16,'Error', 'The settings file appears to be corrupt or missing. Setting defaults.')
		Return
	EndIf

	If $aINI[0][0] = 3 Then
		$Sleep = ($aINI[1][1] * 1000)
		$x = $aINI[2][1]
		$y = $aINI[3][1]
	Else
		ConsoleWrite('Only '&$aINI[0][0]&' settings found. Setting defaults')
		MsgBox(16,'Error', 'The settings file appears to be corrupt or missing. Setting defaults.')
		$Sleep = 180
		$x = 1
		$y = 1
		_wSettings()
	EndIf

	ConsoleWrite("Uploaded INI file:" &@CRLF& _
				 'Sleep = '&$Sleep &@CRLF& _
				 'x = '&$x &@CRLF& _
				 'y = '&$y&@CRLF)
EndFunc

Func _wSettings()


	If $DEBUG = 1 Then
		MsgBox(64,"DEBUG", "wSettings Module")
		Return
	ElseIf $DEBUG = 2 Then
		MsgBox(64,"DEBUG", "wSettings Module")
	Else
		ConsoleWrite("wSettings" & @LF)
	EndIf

	;Build settings INI array
	Local	$aINI[4][2] = [[4, ""],['Sleep', $Sleep/1000], ['x', $x], ['y', $y]]

	IniWriteSection($fINI,'Settings', $aINI)

	ConsoleWrite("Updated INI file:" &@CRLF& _
				 'Sleep = '&$Sleep &@CRLF& _
				 'x = '&$x &@CRLF& _
				 'y = '&$y&@CRLF)

EndFunc

Func _Exit()
	If $DEBUG = 1 Then
		MsgBox(64,"DEBUG", "Exit Module")
		Return
	ElseIf $DEBUG = 2 Then
		MsgBox(64,"DEBUG", "Exit Module")
	Else
		ConsoleWrite("Exit" & @LF)
	EndIf

	Exit
EndFunc
